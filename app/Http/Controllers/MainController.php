<?php

namespace App\Http\Controllers;

use Maatwebsite\Excel\Facades\Excel;
use Yangqi\Htmldom\Htmldom;

class MainController extends Controller
{
    protected $titles = [
        "Grafikchip(Klick zum Artikel)",
        "Max. Speicher-Größein MB"
    ];

    public function main()
    {
        $html = new Htmldom('http://www.pc-erfahrung.de/grafikkarte/vga-grafikrangliste.html');
        if ($html->root !== null) {
            $table_html = $html->getElementById('table_grafikrangliste')->outertext;
            $table_strings = [];
            $strings_found = preg_match_all("#\<tr.*?\<\/tr\>#", $table_html, $table_strings);
            if ($strings_found !== false && !empty($table_strings)) {
                $indexes = $this->getColumnsIndexes($table_strings[0]);
                if ($indexes) {
                    $result_values = $this->getTableValues($table_strings, $indexes);
                    Excel::create('Burning Buttons Test App', function($excel) use ($result_values) {

                        $excel->sheet('First sheet', function($sheet) use ($result_values) {

                            $sheet->setOrientation('landscape');
                            $sheet->fromArray($result_values);

                        });

                    })->export('xls');
                } else {
                    return "Whoops, columns with the passed titles weren't found";
                }
            }
        } else {
            return 'Whoops, can not load the page';
        }
    }

    /**
     * Return array of indexes of columns whose title is in $title array
     * @param array $table_strings
     * @return array
     */
    protected function getColumnsIndexes(array $table_strings)
    {
        $indexes = [];

        foreach ($this->titles as $column_title_index => $column_title) {
            foreach ($table_strings as $table_string) {
                $column_index = $this->getColumnIndex($table_string, $column_title);
                if ($column_index !== false) {
                    $indexes[$column_title_index] = $column_index;
                }
            }
        }

        return $indexes;
    }

    /**
     * Parse a string and return an index of column where the passed $title was found.
     * Return false if it wasn't found.
     * @param string $haystack
     * @param string $title
     * @return bool|int $index
     */
    protected function getColumnIndex(string $haystack, string $title)
    {
        $search_string = strip_tags($haystack, '<th>');
        $contain_title = strpos($search_string, $title);
        $index = false;

        if ($contain_title !== false) {
            $headers = [];
            $match = preg_match_all("#\<th.*?\<\/th\>#", $haystack, $headers);
            if ($match !== false && $headers) {
                foreach ($headers[0] as $column_index => $column_header) {
                    $handled_column_header = strip_tags($column_header);
                    if (strpos($handled_column_header, $title) !== false) {
                        $index = $column_index;
                        break;
                    }
                }
            }
        }

        return $index;
    }

    /**
     * Look for strings with stuff specifications
     * and take data from the columns whose index is in the passed $indexes array.
     * @param array $table_strings
     * @param array $indexes
     * @return array
     */
    protected function getTableValues(array $table_strings, array $indexes)
    {
        $values = [];

        foreach ($table_strings[0] as $table_string) {

            // if this string contain stuff data
            if (strpos($table_string, '<td')) {
                $cells = [];

                // get array of cells
                $match = preg_match_all("#\<td.*?\<\/td\>#", $table_string, $cells);
                if ($match !== false && $cells) {
                    $new_value = [];

                    // go through our cells and check it's index.
                    foreach ($cells[0] as $cell_index => $cell_value) {
                        if (in_array($cell_index, $indexes)) {
                            $new_value[array_search($cell_index, $indexes)] = $this->processCellValue($cell_value);
                        }
                    }

                    if ($new_value) $values[] = $new_value;
                }
            }
        }

        return $values;
    }

    /**
     * Process a cell value before it's shown
     * @param string $cell_value
     * @return string
     */
    protected function processCellValue(string $cell_value)
    {
        // delete score from string
        if ($score_offset = strpos($cell_value, '<div class="score"')) {
            $cell_value = substr($cell_value, 0, $score_offset);
        }

        // delete all tags
        $cell_value = strip_tags($cell_value);

        return trim($cell_value);
    }
}